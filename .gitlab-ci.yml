---

stages:
  - lint
  - build
  - test
  - deploy
  - post deploy
  - publishing

image: openjdk:12-alpine

variables:
  AWS_S3_BUCKET: cars-api-deployments-karma
  ARTIFACT_NAME: cars-api-${CI_PIPELINE_IDD}.jar
  APP_NAME: cars-api
  PATH_TO_PLACEHOLDER: ./src/main/resources/application.yml

build:
  stage: build
  before_script:
    - >-
      sed -i "s/CI_PIPELINE_IID/$CI_PIPELINE_IID/" ${PATH_TO_PLACEHOLDER};
      sed -i "s/CI_COMMIT_SHORT_SHA/$CI_COMMIT_SHORT_SHA/" ${PATH_TO_PLACEHOLDER};
      sed -i "s/CI_COMMIT_BRANCH/$CI_COMMIT_BRANCH/" ${PATH_TO_PLACEHOLDER};
  script:
    - ./gradlew build
    - mv ./build/libs/cars-api.jar ./build/libs/${ARTIFACT_NAME}
  artifacts:
    paths:
      - ./build/libs/

smoke test:
  stage: test
  before_script:
    - apk add --no-cache curl
  script:
    - java -jar ./build/libs/${ARTIFACT_NAME} &
    - sleep 30
    - curl http://localhost:5000/actuator/health | grep "UP"

code quality:
  stage: lint
  script:
    - ./gradlew pmdMain pmdTest
  artifacts:
    when: always
    paths:
      - build/reports/pmd

unit tests:
  stage: test
  script:
    - ./gradlew test
  artifacts:
    when: always
    paths:
      - build/reports/tests
    reports:
      junit: build/test-results/test/*.xml

deploy:
  stage: deploy
  image:
    name: banst/awscli
    entrypoint: [""]
  before_script:
    - apk add --no-cache curl jq
  script:
    - >-
      aws s3 cp ./build/libs/${ARTIFACT_NAME} \
        s3://${AWS_S3_BUCKET}/${ARTIFACT_NAME}
    - >-
      aws elasticbeanstalk create-application-version \
        --application-name ${APP_NAME} \
        --version-label=${CI_PIPELINE_IID} \
        --source-bundle S3Bucket=${AWS_S3_BUCKET},S3Key=${ARTIFACT_NAME}
    - >-
      CNAME=$(aws elasticbeanstalk update-environment \
        --environment-name "Carsapi-env" \
        --version-label=${CI_PIPELINE_IID} | jq '.CNAME' --raw-output)
    - echo $CNAME
    - sleep 45
    - curl http://$CNAME/actuator/info | grep $CI_PIPELINE_IID
    - curl http://$CNAME/actuator/health | grep "UP"

api testing:
  stage: post deploy
  image:
    name: vdespa/newman
    entrypoint: [""]
  script:
    - newman --version
    - newman run "Cars+API.postman_collection.json" --environment Production.postman_environment.json --reporters cli,htmlextra,junit --reporter-htmlextra-export "newman/report.html" --reporter-junit-export "newman/report.xml"
  artifacts:
    when: always
    paths:
      - newman/
    reports:
      junit: newman/report.xml

pages:
  stage: publishing
  script:
    - mkdir public
    - mv newman/report.html public/index.html
  artifacts:
    paths:
      - public
