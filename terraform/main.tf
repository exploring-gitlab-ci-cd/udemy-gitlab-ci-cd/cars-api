provider "aws" {
  region = "eu-central-1"
}

resource "aws_elastic_beanstalk_application" "cars-api" {
  name        = "cars-api"
  description = "cars-api"

  tags = {
    Name = "ca-app"
  }
}

resource "aws_elastic_beanstalk_environment" "cars-api" {
  name                = "production"
  application         = aws_elastic_beanstalk_application.cars-api.name
  solution_stack_name = "64bit Amazon Linux 2 v3.2.4 running Corretto 8"

  tier         = "WebServer"
  cname_prefix = "cars-api-karma"

  tags = {
    Name = "ca-env"
  }
}