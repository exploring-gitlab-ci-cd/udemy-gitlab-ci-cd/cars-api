FROM openjdk:12-alpine

COPY ./build/libs/cars-api.jar ./

ENTRYPOINT ["java", "-jar"]
CMD ["cars-api.jar"]
