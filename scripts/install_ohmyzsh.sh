#!/bin/bash
V_PATH=/home/vagrant
ZSH_CUSTOM=$V_PATH/.oh-my-zsh/custom
sudo apt update -y
sudo apt install zsh -y
git clone https://github.com/ohmyzsh/ohmyzsh.git $V_PATH/ohmyzsh
sudo -i -u vagrant /bin/sh $V_PATH/ohmyzsh/tools/install.sh
rm -rf $V_PATH/ohmyzsh/
git clone https://github.com/zsh-users/zsh-autosuggestions.git $ZSH_CUSTOM/plugins/zsh-autosuggestions
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git $ZSH_CUSTOM/plugins/zsh-syntax-highlighting
sed -i 's/ZSH_THEME="robbyrussell"/ZSH_THEME="aussiegeek"/g' $V_PATH/.zshrc
sed -i 's/plugins=(git)/plugins=(git zsh-autosuggestions zsh-syntax-highlighting)/g' $V_PATH/.zshrc
sudo chsh -s /bin/zsh vagrant
